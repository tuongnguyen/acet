﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using ACET_WEB.Models;
using ACET_WEB.App_Start;

namespace ACET_WEB.Apis
{
    public class UsersController : ApiController
    {
		[Authorize(Roles = "Administrator")]
        public WrapUserSerial GetAllUsers([FromUri] int draw, [FromUri] int length, [FromUri] int start,
            [FromUri] string search_value, [FromUri] string order_colunm, [FromUri] string order_dir)
        {
             
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var rs = new WrapUserSerial();
                var rs_data = new List<List<dynamic>>();
                var users = dc.Users.Where(c => c.Is_Active.Equals(1));
                if (search_value!=null)
                {
                    if (!search_value.Trim().Equals(""))
                    {
                        users = users.Where(c => c.UserName.Contains(search_value) || c.Email.Contains(search_value));
                    }
                }
                switch (order_colunm)
                {
                    case "0":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }
                        break;
                    case "1":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.UserName);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.UserName);
                        }
                        break;
                    case "2":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Email);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Email);
                        }
                        break;
                    case "3":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.modified);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.modified);
                        }
                        break;
                    default:
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);    
                        }
                        
                        break;
                }
              
                foreach (var user in users.ToArray())
                {
                    var item = new List<dynamic>();
                    item.Add(user.Id.ToString());
                    item.Add(user.UserName);
                    item.Add(user.Email);
                    item.Add(user.modified.ToShortDateString());
                   // item.Add(user.Is_Active.ToString());
                    string roles_str = "";
                    foreach (var ro in user.UserRoles.ToArray())
                    {
                        roles_str += ro.Role.RoleName + ",";
                    }
                    item.Add(roles_str.Substring(0, roles_str.Length - 1));
                    rs_data.Add(item);
                }
                rs.draw = draw;
                rs.recordsFiltered = rs_data.Count();
                rs.recordsTotal = rs_data.Count();
                rs.data = rs_data;
                return rs;
            }
        }
        public UserSerial Get(int id)
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var user = dc.Users.FirstOrDefault(c => c.Id.Equals(id));
                if (user == null)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
                else {
                    var item = new UserSerial();
                    item.Id = user.Id;
                    item.UserName = user.UserName;
                    item.FirstName = user.FirstName;
                    item.LastName = user.LastName;
                    item.Email = user.Email;
                    item.Modified = user.modified;
                    item.IsActive = user.Is_Active;
                    item.Roles = new List<int>();
                    foreach (var ro in user.UserRoles.ToArray())
                    {
                        item.Roles.Add(ro.Role.Id);
                    }
                    return item;
                }
            }
        }
        public class UserForm
        {
            public int id { get; set; }
            public string username { get; set; }
            public string email { get; set; }
            public string password { get; set; }
            public int role_id { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
        }
        public Boolean Post(int id) {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                try
                {   
                    var u = dc.Users.FirstOrDefault(c => c.Id.Equals(id));
                    u.UserRoles.Clear();
                    dc.Users.DeleteObject(u);
                    dc.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }
        public Boolean Post([FromBody] UserForm userform)
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                try
                {
                    if (userform.id>0)
                    {
                        var u = dc.Users.FirstOrDefault(c => c.Id.Equals(userform.id));
                        u.modified = DateTime.Now;
                        u.LastName = userform.lastname;
                        u.FirstName = userform.firstname;
                        u.UserRoles.Clear();
                        var ro = dc.Roles.FirstOrDefault(r => r.Id.Equals(userform.role_id));
                        u.UserRoles.Add(new UserRole
                        {
                            Role = ro
                        });
                        dc.SaveChanges();
                        return true;
                    }
                    else
                    {
                        User u = new User();
                        u.UserName = userform.username;
                        u.Email = userform.email;
                        u.Password = Helper.EncodeSHA1(userform.password);
                        var ro = dc.Roles.FirstOrDefault(r => r.Id.Equals(userform.role_id));
                        u.created = DateTime.Now;
                        u.modified = DateTime.Now;
                        u.LastName = userform.lastname;
                        u.FirstName = userform.firstname;
                        u.Is_Active = 1;
                        u.UserRoles.Add(new UserRole
                        {
                            Role = ro
                        });
                        dc.Users.AddObject(u);
                        dc.SaveChanges();
                        return true;
                    }  
                }
                catch (Exception ex)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }   
            }
        }
    }
}
