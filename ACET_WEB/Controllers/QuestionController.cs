﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Controllers
{
	[Authorize(Roles = "Administrator")]
	public class QuestionController : Controller
    {
        private acet_dbEntities db = new acet_dbEntities();

        //
        // GET: /Question/

        public ActionResult Index(int group=0)
        {
            ViewBag.gid = group;
            var questions = db.Questions.Include("QuestionGroup");
            if (db.QuestionGroups.FirstOrDefault(p=>p.Id==group)!=null)
            {
                
                return View(questions.Where(q => q.QuestionGroup.Id == group).ToList());
            }
            return View(questions.ToList());
        }

        //
        // GET: /Question/Details/5

        public ActionResult Details(int id = 0)
        {
            Question question = db.Questions.Single(q => q.Id == id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        //
        // GET: /Question/Create

        public ActionResult Create(int group = 0)
        {
            ViewBag.gid = group;
            var qg = db.QuestionGroups.FirstOrDefault(p => p.Id == group);
            if (qg != null)
            {
                ViewBag.GroupId = new SelectList(db.QuestionGroups, "Id", "Name", qg.Id);
            }
            else
            {
                ViewBag.GroupId = new SelectList(db.QuestionGroups, "Id", "Name");
            }
           
            return View();
        }

        //
        // POST: /Question/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Question question)
        {
            if (ModelState.IsValid)
            {
                db.Questions.AddObject(question);
                db.SaveChanges();
                return RedirectToAction("Index", new { group=question.GroupId});
            }

            ViewBag.GroupId = new SelectList(db.QuestionGroups, "Id", "Name", question.GroupId);
            return View(question);
        }

        //
        // GET: /Question/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Question question = db.Questions.Single(q => q.Id == id);
            if (question == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.QuestionGroups, "Id", "Name", question.GroupId);
            return View(question);
        }

        //
        // POST: /Question/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Question question)
        {
            if (ModelState.IsValid)
            {
                db.Questions.Attach(question);
                db.ObjectStateManager.ChangeObjectState(question, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index", new { group = question.GroupId });
            }
            ViewBag.GroupId = new SelectList(db.QuestionGroups, "Id", "Name", question.GroupId);
            return View(question);
        }

        //
        // GET: /Question/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Question question = db.Questions.Single(q => q.Id == id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        //
        // POST: /Question/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Questions.Single(q => q.Id == id);
            var group = question.GroupId;
            db.Questions.DeleteObject(question);
            db.SaveChanges();
            return RedirectToAction("Index", new { group = group });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}