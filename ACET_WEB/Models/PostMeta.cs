﻿using System;

namespace ACET_WEB
{
	public class PostMeta
	{
		public int Id { get; set; }
		public int PostId { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
		public string lang { get; set;}
		 
	}
}

