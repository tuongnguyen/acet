﻿CREATE FUNCTION [dbo].[fn_nosymbol]
(
@input_string NVARCHAR (max)
)
RETURNS VARCHAR (max)
AS
BEGIN
DECLARE @l_str NVARCHAR(max);
SET @l_str = LTRIM(@input_string);
SET @l_str = LOWER(RTRIM(@l_str));
 
SET @l_str = REPLACE(@l_str, N'á', 'a');
 
SET @l_str = REPLACE(@l_str, N'à', 'a');
SET @l_str = REPLACE(@l_str, N'ả', 'a');
SET @l_str = REPLACE(@l_str, N'ã', 'a');
SET @l_str = REPLACE(@l_str, N'ạ', 'a');
 
SET @l_str = REPLACE(@l_str, N'â', 'a');
SET @l_str = REPLACE(@l_str, N'ấ', 'a');
SET @l_str = REPLACE(@l_str, N'ầ', 'a');
SET @l_str = REPLACE(@l_str, N'ẩ', 'a');
SET @l_str = REPLACE(@l_str, N'ẫ', 'a');
SET @l_str = REPLACE(@l_str, N'ậ', 'a');
 
SET @l_str = REPLACE(@l_str, N'ă', 'a');
SET @l_str = REPLACE(@l_str, N'ắ', 'a');
SET @l_str = REPLACE(@l_str, N'ằ', 'a');
SET @l_str = REPLACE(@l_str, N'ẳ', 'a');
SET @l_str = REPLACE(@l_str, N'ẵ', 'a');
SET @l_str = REPLACE(@l_str, N'ặ', 'a');
 
SET @l_str = REPLACE(@l_str, N'é', 'e');
SET @l_str = REPLACE(@l_str, N'è', 'e');
SET @l_str = REPLACE(@l_str, N'ẻ', 'e');
SET @l_str = REPLACE(@l_str, N'ẽ', 'e');
SET @l_str = REPLACE(@l_str, N'ẹ', 'e');
 
SET @l_str = REPLACE(@l_str, N'ê', 'e');
SET @l_str = REPLACE(@l_str, N'ế', 'e');
SET @l_str = REPLACE(@l_str, N'ề', 'e');
SET @l_str = REPLACE(@l_str, N'ể', 'e');
SET @l_str = REPLACE(@l_str, N'ễ', 'e');
SET @l_str = REPLACE(@l_str, N'ệ', 'e');
 
SET @l_str = REPLACE(@l_str, N'í', 'i');
SET @l_str = REPLACE(@l_str, N'ì', 'i');
SET @l_str = REPLACE(@l_str, N'ỉ', 'i');
SET @l_str = REPLACE(@l_str, N'ĩ', 'i');
SET @l_str = REPLACE(@l_str, N'ị', 'i');
 
SET @l_str = REPLACE(@l_str, N'ó', 'o');
SET @l_str = REPLACE(@l_str, N'ò', 'o');
SET @l_str = REPLACE(@l_str, N'ỏ', 'o');
SET @l_str = REPLACE(@l_str, N'õ', 'o');
SET @l_str = REPLACE(@l_str, N'ọ', 'o');
 
SET @l_str = REPLACE(@l_str, N'ô', 'o');
SET @l_str = REPLACE(@l_str, N'ố', 'o');
SET @l_str = REPLACE(@l_str, N'ồ', 'o');
SET @l_str = REPLACE(@l_str, N'ổ', 'o');
SET @l_str = REPLACE(@l_str, N'ỗ', 'o');
SET @l_str = REPLACE(@l_str, N'ộ', 'o');
 
SET @l_str = REPLACE(@l_str, N'ơ', 'o');
SET @l_str = REPLACE(@l_str, N'ớ', 'o');
SET @l_str = REPLACE(@l_str, N'ờ', 'o');
SET @l_str = REPLACE(@l_str, N'ở', 'o');
SET @l_str = REPLACE(@l_str, N'ỡ', 'o');
SET @l_str = REPLACE(@l_str, N'ợ', 'o');
 
SET @l_str = REPLACE(@l_str, N'ú', 'u');
SET @l_str = REPLACE(@l_str, N'ù', 'u');
SET @l_str = REPLACE(@l_str, N'ủ', 'u');
SET @l_str = REPLACE(@l_str, N'ũ', 'u');
SET @l_str = REPLACE(@l_str, N'ụ', 'u');
 
SET @l_str = REPLACE(@l_str, N'ư', 'u');
SET @l_str = REPLACE(@l_str, N'ứ', 'u');
SET @l_str = REPLACE(@l_str, N'ừ', 'u');
SET @l_str = REPLACE(@l_str, N'ử', 'u');
SET @l_str = REPLACE(@l_str, N'ữ', 'u');
SET @l_str = REPLACE(@l_str, N'ự', 'u');
 
SET @l_str = REPLACE(@l_str, N'ý', 'y');
SET @l_str = REPLACE(@l_str, N'ỳ', 'y');
SET @l_str = REPLACE(@l_str, N'ỷ', 'y');
SET @l_str = REPLACE(@l_str, N'ỹ', 'y');
SET @l_str = REPLACE(@l_str, N'ỵ', 'y');
 
SET @l_str = REPLACE(@l_str, N'đ', 'd');
 
 
--SET @l_str = REPLACE(@l_str, ' ', '-');
--SET @l_str = REPLACE(@l_str, '~', '-');
--SET @l_str = REPLACE(@l_str, '?', '-');
--SET @l_str = REPLACE(@l_str, '@', '-');
--SET @l_str = REPLACE(@l_str, '#', '-');
--SET @l_str = REPLACE(@l_str, '$', '-');
--SET @l_str = REPLACE(@l_str, '^', '-');
--SET @l_str = REPLACE(@l_str, '&', '-');
--SET @l_str = REPLACE(@l_str, '/', '-');
 
--SET @l_str = REPLACE(@l_str, '(', '');
--SET @l_str = REPLACE(@l_str, ')', '');
--SET @l_str = REPLACE(@l_str, '[', '');
--SET @l_str = REPLACE(@l_str, ']', '');
--SET @l_str = REPLACE(@l_str, '{', '');
--SET @l_str = REPLACE(@l_str, '}', '');
--SET @l_str = REPLACE(@l_str, '<', '');
--SET @l_str = REPLACE(@l_str, '>', '');
--SET @l_str = REPLACE(@l_str, '|', '');
--SET @l_str = REPLACE(@l_str, ''', '');
--SET @l_str = REPLACE(@l_str, '%', '');
--SET @l_str = REPLACE(@l_str, '^', '');
--SET @l_str = REPLACE(@l_str, '*', '');
--SET @l_str = REPLACE(@l_str, '!', '');
--SET @l_str = REPLACE(@l_str, ',', '');
--SET @l_str = REPLACE(@l_str, '.', '');
 
--SET @l_str = REPLACE(@l_str, '---', '-');
--SET @l_str = REPLACE(@l_str, '--', '-');
 
RETURN @l_str;
END


ALTER TABLE [dbo].[Posts]
    ADD [ContentIndex]    NVARCHAR (MAX) NULL,
        [ContentIndexEnd] NVARCHAR (MAX)    NULL;

ALTER TABLE [dbo].[Posts] 
	ALTER COLUMN [Status] INT NULL;
ALTER TABLE [dbo].[Posts] ADD DEFAULT 1 FOR  [Status];

update Posts set Status=1;

update Posts set ContentIndex = dbo.fn_nosymbol(Title) + ' '+ dbo.fn_nosymbol(Content);

ALTER TABLE [dbo].[Posts]
    ADD [CusPoint]    DECIMAL (18) DEFAULT 0 NULL,
        [IsPromotion] INT          DEFAULT 0 NULL;

ALTER TABLE [dbo].[Categories]
    ADD [Img] NVARCHAR (250) NULL,
        [ImgMobile] NVARCHAR (250) NULL,
        [Status] INT DEFAULT 1 NULL;


ALTER TABLE [dbo].[QuestionGroup]
    ADD [Type] INT NULL;

insert into [dbo].[Categories] (Name,NameEng) values ('First Steps','First Steps'), ('Anh ngữ Học thuật','Academic English'), ('Luyện thi IELTS','IELTS')


----------

USE [ACET_DB_NEW]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_nosymbol]    Script Date: 10/14/2015 2:50:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[fn_nosymbol]
(
@input_string NVARCHAR (max)
)
RETURNS VARCHAR (max)
AS
BEGIN
DECLARE @l_str NVARCHAR(max);
SET @l_str = LTRIM(@input_string);
SET @l_str = RTRIM(@l_str);
 
SET @l_str = REPLACE(@l_str, N'á', 'a');
 
SET @l_str = REPLACE(@l_str, N'à', 'a');
SET @l_str = REPLACE(@l_str, N'ả', 'a');
SET @l_str = REPLACE(@l_str, N'ã', 'a');
SET @l_str = REPLACE(@l_str, N'ạ', 'a');
 
SET @l_str = REPLACE(@l_str, N'â', 'a');
SET @l_str = REPLACE(@l_str, N'ấ', 'a');
SET @l_str = REPLACE(@l_str, N'ầ', 'a');
SET @l_str = REPLACE(@l_str, N'ẩ', 'a');
SET @l_str = REPLACE(@l_str, N'ẫ', 'a');
SET @l_str = REPLACE(@l_str, N'ậ', 'a');
 
SET @l_str = REPLACE(@l_str, N'ă', 'a');
SET @l_str = REPLACE(@l_str, N'ắ', 'a');
SET @l_str = REPLACE(@l_str, N'ằ', 'a');
SET @l_str = REPLACE(@l_str, N'ẳ', 'a');
SET @l_str = REPLACE(@l_str, N'ẵ', 'a');
SET @l_str = REPLACE(@l_str, N'ặ', 'a');
 
SET @l_str = REPLACE(@l_str, N'é', 'e');
SET @l_str = REPLACE(@l_str, N'è', 'e');
SET @l_str = REPLACE(@l_str, N'ẻ', 'e');
SET @l_str = REPLACE(@l_str, N'ẽ', 'e');
SET @l_str = REPLACE(@l_str, N'ẹ', 'e');
 
SET @l_str = REPLACE(@l_str, N'ê', 'e');
SET @l_str = REPLACE(@l_str, N'ế', 'e');
SET @l_str = REPLACE(@l_str, N'ề', 'e');
SET @l_str = REPLACE(@l_str, N'ể', 'e');
SET @l_str = REPLACE(@l_str, N'ễ', 'e');
SET @l_str = REPLACE(@l_str, N'ệ', 'e');
 
SET @l_str = REPLACE(@l_str, N'í', 'i');
SET @l_str = REPLACE(@l_str, N'ì', 'i');
SET @l_str = REPLACE(@l_str, N'ỉ', 'i');
SET @l_str = REPLACE(@l_str, N'ĩ', 'i');
SET @l_str = REPLACE(@l_str, N'ị', 'i');
 
SET @l_str = REPLACE(@l_str, N'ó', 'o');
SET @l_str = REPLACE(@l_str, N'ò', 'o');
SET @l_str = REPLACE(@l_str, N'ỏ', 'o');
SET @l_str = REPLACE(@l_str, N'õ', 'o');
SET @l_str = REPLACE(@l_str, N'ọ', 'o');
 
SET @l_str = REPLACE(@l_str, N'ô', 'o');
SET @l_str = REPLACE(@l_str, N'ố', 'o');
SET @l_str = REPLACE(@l_str, N'ồ', 'o');
SET @l_str = REPLACE(@l_str, N'ổ', 'o');
SET @l_str = REPLACE(@l_str, N'ỗ', 'o');
SET @l_str = REPLACE(@l_str, N'ộ', 'o');
 
SET @l_str = REPLACE(@l_str, N'ơ', 'o');
SET @l_str = REPLACE(@l_str, N'ớ', 'o');
SET @l_str = REPLACE(@l_str, N'ờ', 'o');
SET @l_str = REPLACE(@l_str, N'ở', 'o');
SET @l_str = REPLACE(@l_str, N'ỡ', 'o');
SET @l_str = REPLACE(@l_str, N'ợ', 'o');
 
SET @l_str = REPLACE(@l_str, N'ú', 'u');
SET @l_str = REPLACE(@l_str, N'ù', 'u');
SET @l_str = REPLACE(@l_str, N'ủ', 'u');
SET @l_str = REPLACE(@l_str, N'ũ', 'u');
SET @l_str = REPLACE(@l_str, N'ụ', 'u');
 
SET @l_str = REPLACE(@l_str, N'ư', 'u');
SET @l_str = REPLACE(@l_str, N'ứ', 'u');
SET @l_str = REPLACE(@l_str, N'ừ', 'u');
SET @l_str = REPLACE(@l_str, N'ử', 'u');
SET @l_str = REPLACE(@l_str, N'ữ', 'u');
SET @l_str = REPLACE(@l_str, N'ự', 'u');
 
SET @l_str = REPLACE(@l_str, N'ý', 'y');
SET @l_str = REPLACE(@l_str, N'ỳ', 'y');
SET @l_str = REPLACE(@l_str, N'ỷ', 'y');
SET @l_str = REPLACE(@l_str, N'ỹ', 'y');
SET @l_str = REPLACE(@l_str, N'ỵ', 'y');
 
SET @l_str = REPLACE(@l_str, N'đ', 'd');
 
 
SET @l_str = REPLACE(@l_str, ' ', '-');
SET @l_str = REPLACE(@l_str, '~', '');
SET @l_str = REPLACE(@l_str, '?', '');
SET @l_str = REPLACE(@l_str, '@', '');
SET @l_str = REPLACE(@l_str, '#', '');
SET @l_str = REPLACE(@l_str, '$', '');
SET @l_str = REPLACE(@l_str, '^', '');
SET @l_str = REPLACE(@l_str, '&', '');
SET @l_str = REPLACE(@l_str, '/', '');
 
SET @l_str = REPLACE(@l_str, '(', '');
SET @l_str = REPLACE(@l_str, ')', '');
SET @l_str = REPLACE(@l_str, '[', '');
SET @l_str = REPLACE(@l_str, ']', '');
SET @l_str = REPLACE(@l_str, '{', '');
SET @l_str = REPLACE(@l_str, '}', '');
SET @l_str = REPLACE(@l_str, '<', '');
SET @l_str = REPLACE(@l_str, '>', '');
SET @l_str = REPLACE(@l_str, '|', '');
SET @l_str = REPLACE(@l_str, '"', '');
SET @l_str = REPLACE(@l_str, '%', '');
SET @l_str = REPLACE(@l_str, '^', '');
SET @l_str = REPLACE(@l_str, '*', '');
SET @l_str = REPLACE(@l_str, '!', '');
SET @l_str = REPLACE(@l_str, ',', '');
SET @l_str = REPLACE(@l_str, '.', '');
SET @l_str = REPLACE(@l_str, ':', '');
SET @l_str = REPLACE(@l_str, '\''', '');
SET @l_str = REPLACE(@l_str, '---', '-');
SET @l_str = REPLACE(@l_str, '--', '-');
 
RETURN @l_str;
END


update Categories set Slug = replace(lower([dbo].fn_nosymbol(Name)),' ','-'), SlugEng=replace(lower([dbo].fn_nosymbol(Name)),' ','-')
update Posts set Slug = lower([dbo].fn_nosymbol(Title)), SlugEng=lower([dbo].fn_nosymbol(TitleEng))


ALTER TABLE [dbo].[Question]
    ADD [AnswerKey] VARBINARY (50) NULL;
ALTER TABLE [dbo].[Question] DROP COLUMN [Type];
ALTER TABLE [dbo].[Question] ALTER COLUMN [AnswerKey] NVARCHAR (50) NULL;


CREATE TABLE [dbo].[Media] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Title]      NVARCHAR (250) NULL,
    [TitleEng]   NVARCHAR (250) NULL,
    [Link]       NVARCHAR (250) NULL,
    [CategoryId] INT            NULL,
    [Status]     INT            NULL,
    [Created]    DATETIME       NULL,
    [Modified]   DATETIME       NULL,
    [Type]       NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[MediaCategory] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (250) NULL,
    [NameEng]  NVARCHAR (250) NULL,
    [Status]   INT            NULL,
    [Created]  DATETIME       NULL,
    [Modified] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

ALTER TABLE [dbo].[Media] WITH NOCHECK
    ADD CONSTRAINT [FK_Media_MediaCategory] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[MediaCategory] ([Id]);
ALTER TABLE [dbo].[MediaCategory]
    ADD [IsHome] BIT NULL;
ALTER TABLE [dbo].[MediaCategory]
    ADD [Img] NVARCHAR (MAX) NULL;