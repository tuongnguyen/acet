﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ACET_WEB.App_Start
{
    public class LocalizationAttribute : ActionFilterAttribute
    {
        private string _DefaultLanguage = "vi";

        public LocalizationAttribute(string defaultLanguage)
        {
            _DefaultLanguage = defaultLanguage;
        }
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
			using (acet_dbEntities dc = new acet_dbEntities ()) {
				var menu_top = dc.Menus.Include ("Menus1").Where (p => p.Type.Equals ("top") && p.ParentId == null).OrderBy (p => p.Order);
				var menu_bottom = dc.Menus.Include ("Menus1").Where (p => p.Type.Equals ("bottom") && p.ParentId == null).OrderBy (p => p.Order);
				filterContext.Controller.ViewBag.menu_top = menu_top.ToList ();
				filterContext.Controller.ViewBag.menu_bottom = menu_bottom.ToList ();

				HttpCookie cookie = HttpContext.Current.Request.Cookies ["curlang"];
				string lang = (string)filterContext.RouteData.Values ["lang"] ?? "";
            
				if (!lang.Equals ("")) {
					cookie = new HttpCookie ("curlang", lang);
					cookie.Expires = DateTime.Now.AddDays (30);
					filterContext.HttpContext.Response.Cookies.Set (cookie);
					if (filterContext.Controller.ViewBag.lang == null) {
						filterContext.Controller.ViewBag.lang = lang;
					}
                
				} else {
					string hl = (string)filterContext.HttpContext.Request.QueryString ["hl"] ?? "";
					if (!hl.Equals ("")) {
						cookie = new HttpCookie ("curlang", hl);
						cookie.Expires = DateTime.Now.AddDays (30);
						filterContext.HttpContext.Response.Cookies.Set (cookie);
					} else {
						if (cookie == null) {
							cookie = new HttpCookie ("curlang", _DefaultLanguage);
						}
						cookie.Expires = DateTime.Now.AddDays (30);
						filterContext.HttpContext.Response.Cookies.Set (cookie);
					}
					if (filterContext.Controller.ViewBag.lang == null) {
						filterContext.Controller.ViewBag.lang = cookie.Value;
					}
				}
				if (filterContext.Controller.ViewBag.lang.Equals ("vi")) {
					filterContext.Controller.ViewBag.HomeLeft = dc.Modules.Where (m => m.Position.Equals ("Home Left")).OrderBy (m => m.Order).ToList ();
					filterContext.Controller.ViewBag.HomeRight = dc.Modules.Where (m => m.Position.Equals ("Home Right")).OrderBy (m => m.Order).ToList ();
					filterContext.Controller.ViewBag.PageRight = dc.Modules.Where (m => m.Position.Equals ("Page Right")).OrderBy (m => m.Order).ToList ();
					filterContext.Controller.ViewBag.HomeBottom = dc.Modules.Where (m => m.Position.Equals ("Home Bottom")).OrderBy (m => m.Order).Skip(0).Take(3).ToList ();
					filterContext.Controller.ViewBag.PageBottom = dc.Modules.Where (m => m.Position.Equals ("Page Bottom")).OrderBy (m => m.Order).Skip(0).Take(3).ToList ();
					filterContext.Controller.ViewBag.HomeAddress = dc.Modules.Where (m => m.Position.Equals ("Home Address")).OrderBy (m => m.Order).Skip(0).Take(1).ToList ();
					filterContext.Controller.ViewBag.CenterBar = dc.Modules.Where (m => m.Position.Equals ("CenterBar")).OrderBy (m => m.Order).FirstOrDefault ();

				} else {
					filterContext.Controller.ViewBag.HomeLeft = dc.Modules.Where (m => m.Position.Equals ("Home Left English")).OrderBy (m => m.Order).ToList ();
					filterContext.Controller.ViewBag.HomeRight = dc.Modules.Where (m => m.Position.Equals ("Home Right English")).OrderBy (m => m.Order).ToList ();
					filterContext.Controller.ViewBag.PageRight = dc.Modules.Where (m => m.Position.Equals ("Page Right English")).OrderBy (m => m.Order).ToList ();
					filterContext.Controller.ViewBag.HomeBottom = dc.Modules.Where (m => m.Position.Equals ("Home Bottom English")).OrderBy (m => m.Order).Skip(0).Take(3).ToList ();
					filterContext.Controller.ViewBag.PageBottom = dc.Modules.Where (m => m.Position.Equals ("Page Bottom English")).OrderBy (m => m.Order).Skip(0).Take(3).ToList ();
					filterContext.Controller.ViewBag.HomeAddress = dc.Modules.Where (m => m.Position.Equals ("Home Address English")).OrderBy (m => m.Order).Skip(0).Take(1).ToList ();
					filterContext.Controller.ViewBag.CenterBar = dc.Modules.Where (m => m.Position.Equals ("CenterBar English")).OrderBy (m => m.Order).FirstOrDefault ();

				}
				filterContext.Controller.ViewBag.HotlineHN = dc.Modules.Where (m => m.Position.Equals ("Contact Hotline HN")).OrderBy (m => m.Order).FirstOrDefault ();
				filterContext.Controller.ViewBag.HotlineHCM = dc.Modules.Where (m => m.Position.Equals ("Contact Hotline HCM")).OrderBy (m => m.Order).FirstOrDefault ();
				filterContext.Controller.ViewBag.HeaderTag = dc.Modules.Where (m => m.Position.Equals ("Header Tag")).OrderBy (m => m.Order).ToList();
				filterContext.Controller.ViewBag.BodyTag = dc.Modules.Where (m => m.Position.Equals ("Body Tag")).OrderBy (m => m.Order).ToList();

				filterContext.Controller.ViewBag.metadesc = "ACET trung tâm anh ngữ tại Hà Nội và Tp. Hồ Chí Minh các khóa học anh ngữ học thuật, frist step, luyện thi Ielts môi trường học tập hiện đại";
				filterContext.Controller.ViewBag.keywords = "Trung tâm anh ngữ, trung tâm Acet, anh ngữ học thuật, luyện thi ielts, ielts pro A, ielts pro B, anh văn thiếu nhi, thi ielts";
			}
        }
    }
}
