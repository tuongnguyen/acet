﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using ACET_WEB.Models;
using ACET_WEB.App_Start;

namespace ACET_WEB.Apis
{
	public class BannerForm {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Img { get; set; }
		public string Link { get; set; }
		public string Lang { get; set; }
		public string Order { get; set; }
		public string Position { get; set; }
	}
	[Authorize(Roles = "Administrator")]
	public class BannersController : ApiController
	{
		public WrapUserSerial GetAllUsers([FromUri] int draw, [FromUri] int length, [FromUri] int start,
			[FromUri] string search_value, [FromUri] string order_colunm, [FromUri] string order_dir)
		{

			using (acet_dbEntities dc = new acet_dbEntities())
			{
				var rs = new WrapUserSerial();
				var rs_data = new List<List<dynamic>>();
				var users = dc.ChatAgents.Where (p => p.Id > 0);
				if (search_value!=null)
				{
					if (!search_value.Trim().Equals(""))
					{
						users = users.Where(c => c.FirstName.Contains(search_value) || c.LastName.Contains(search_value));
					}
				}
				switch (order_colunm)
				{
				case "0":
					if (order_dir.Equals("asc"))
					{
						users = users.OrderBy(c => c.Id);
					}
					else
					{
						users = users.OrderByDescending(c => c.Id);
					}
					break;
				case "1":
					if (order_dir.Equals("asc"))
					{
						users = users.OrderBy(c => c.FirstName);
					}
					else
					{
						users = users.OrderByDescending(c => c.FirstName);
					}
					break;
				
				default:
					if (order_dir.Equals("asc"))
					{
						users = users.OrderBy(c => c.Id);
					}
					else
					{
						users = users.OrderByDescending(c => c.Id);    
					}

					break;
				}
				var filter = users;
				filter = filter.Skip(start).Take(length);
				foreach (var user in filter.ToArray())
				{
					var item = new List<dynamic>();
					item.Add(user.Id.ToString());
					item.Add(user.FirstName);
					item.Add(user.LastName);
					item.Add(user.Email);
					item.Add(user.Modified);
					item.Add(user.Role);
					rs_data.Add(item);
				}
				rs.draw = draw;
				rs.recordsFiltered = users.Count();
				rs.recordsTotal = users.Count();
				rs.data = rs_data;
				return rs;
			}
		}
		public BannerForm Get(int id)
		{
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				var user = dc.ChatAgents.FirstOrDefault(c => c.Id.Equals(id));
				if (user == null)
				{
					throw new HttpResponseException(HttpStatusCode.NotFound);
				}
				else {
					BannerForm rs = new BannerForm ();
					rs.Id = user.Id;
					rs.Name = user.FirstName;
					rs.Img = user.LastName;
					rs.Link = user.DisplayName;
					rs.Position = user.Email;
					rs.Order = user.Role;
					rs.Lang = user.Status.ToString ();
					return rs;
				}
			}
		}

		public Boolean Post(int id) {
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{   
					var u = dc.ChatAgents.FirstOrDefault(c => c.Id.Equals(id));

					dc.ChatAgents.DeleteObject(u);
					dc.SaveChanges();
					return true;
				}
				catch (Exception ex)
				{
					throw ex;
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
				}
			}
		}
		public Boolean Post([FromBody] BannerForm userform)
		{
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{
					if (userform.Id>0)
					{
						var u = dc.ChatAgents.FirstOrDefault(c => c.Id.Equals(userform.Id));
						u.Modified = DateTime.Now;
						u.FirstName = userform.Name;
						u.LastName = userform.Img;
						u.DisplayName = userform.Link;
						u.Email = userform.Position;
						u.Role = userform.Order;
						u.Status = 0;
						if(userform.Lang.Equals("1"))
							u.Status = 1;
						
						dc.SaveChanges();
						return true;
					}
					else
					{
						ChatAgent u = new ChatAgent();
						Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

						u.Id =unixTimestamp;
						u.FirstName = userform.Name;
						u.LastName = userform.Img;
						u.DisplayName = userform.Link;
						u.Email = userform.Position;
						u.Role = userform.Order;
						u.Status = 0;
						if(userform.Lang.Equals("1"))
							u.Status = 1;
						u.Created = DateTime.Now;
						u.Modified = DateTime.Now;

						dc.ChatAgents.AddObject(u);
						dc.SaveChanges();
						return true;
					}  
				}
				catch (Exception ex)
				{
					throw ex;
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
				}   
			}
		}
	}
}

