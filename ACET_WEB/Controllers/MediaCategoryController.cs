﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Controllers
{
	[Authorize(Roles = "Administrator")]
    public class MediaCategoryController : Controller
    {
        private acet_dbEntities db = new acet_dbEntities();

        //
        // GET: /MediaCategory/

        public ActionResult Index()
        {
            return View(db.MediaCategories.ToList());
        }

        //
        // GET: /MediaCategory/Details/5

        public ActionResult Details(int id = 0)
        {
            MediaCategory mediacategory = db.MediaCategories.Single(m => m.Id == id);
            if (mediacategory == null)
            {
                return HttpNotFound();
            }
            return View(mediacategory);
        }

        //
        // GET: /MediaCategory/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MediaCategory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MediaCategory mediacategory)
        {
            if (ModelState.IsValid)
            {
				mediacategory.Created = DateTime.Now;
				mediacategory.Modified = DateTime.Now;
				mediacategory.IsHome = true;
                db.MediaCategories.AddObject(mediacategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mediacategory);
        }

        //
        // GET: /MediaCategory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MediaCategory mediacategory = db.MediaCategories.Single(m => m.Id == id);
            if (mediacategory == null)
            {
                return HttpNotFound();
            }
            return View(mediacategory);
        }

        //
        // POST: /MediaCategory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MediaCategory mediacategory)
        {
            if (ModelState.IsValid)
            {
                db.MediaCategories.Attach(mediacategory);
				if (mediacategory.Id == 12) {
					mediacategory.IsHome = false;
				} else {
				}
                db.ObjectStateManager.ChangeObjectState(mediacategory, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mediacategory);
        }

        //
        // GET: /MediaCategory/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MediaCategory mediacategory = db.MediaCategories.Single(m => m.Id == id);
            if (mediacategory == null)
            {
                return HttpNotFound();
            }
            return View(mediacategory);
        }

        //
        // POST: /MediaCategory/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MediaCategory mediacategory = db.MediaCategories.Single(m => m.Id == id);
            db.MediaCategories.DeleteObject(mediacategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}