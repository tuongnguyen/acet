﻿using System;
using System.Xml;

namespace ACET_WEB
{
	public class SiteMapFeedGenerator
	{
		XmlTextWriter writer;

		public SiteMapFeedGenerator(System.IO.Stream stream, System.Text.Encoding encoding)
		{
			writer = new XmlTextWriter(stream, encoding);
			writer.Formatting = Formatting.Indented;
		}

		public SiteMapFeedGenerator(System.IO.TextWriter w)
		{
			writer = new XmlTextWriter(w);
			writer.Formatting = Formatting.Indented;
		}
		/// <summary>
		/// Writes the beginning of the SiteMap document
		/// </summary>
		public void WriteStartDocument()
		{
			writer.WriteStartDocument();
			writer.WriteStartElement("urlset");

			writer.WriteAttributeString("xmlns", "http://www.google.com/schemas/sitemap/0.84");
		}
		/// <summary>
		/// Writes the beginning of the SiteMapIndex document
		/// </summary>
		public void WriteStartDocumentIndex()
		{
			writer.WriteStartDocument();
			writer.WriteStartElement("sitemapindex");

			writer.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
		}
		/// <summary>
		/// Writes the beginning of the RSS document
		/// </summary>
		public void WriteStartDocumentRSS(string title, string link, string desc, string host)
		{
			writer.WriteStartDocument();
			writer.WriteStartElement("rss");

			writer.WriteAttributeString("version", "2.0");

			writer.WriteStartElement("channel");
			writer.WriteElementString("title", title);
			writer.WriteElementString("link", link);
			writer.WriteElementString("description", desc);
			writer.WriteElementString("copyright", "Copyright 2016 "+ host +". All rights reserved.");


		}
		/// <summary>
		/// Writes the end of the SiteMap document
		/// </summary>
		public void WriteEndDocument()
		{
			writer.WriteEndElement();
			writer.WriteEndDocument();
		}
		/// <summary>
		/// Writes the end of the RSS document
		/// </summary>
		public void WriteEndDocumentRSS()
		{
			writer.WriteEndElement();
			writer.WriteEndElement();
			writer.WriteEndDocument();
		}
		/// <summary>
		/// Closes this stream and the underlying stream
		/// </summary>
		public void Close()
		{
			writer.Flush();
			writer.Close();
		}
		public void WriteItemIndex(string link, DateTime publishedDate, string priority, string changefreq)
		{
			writer.WriteStartElement("sitemap");
			writer.WriteElementString("loc", link);
			writer.WriteElementString("lastmod", publishedDate.ToString("yyyy-MM-dd"));
			writer.WriteEndElement();
		}
		public void WriteItemRSS(string link, DateTime publishedDate, string title, string description)
		{
			writer.WriteStartElement("item");
			writer.WriteElementString("title", title);
			writer.WriteElementString("description", description);
			writer.WriteElementString("link", link);
			writer.WriteElementString("pubDate", publishedDate.ToString("yyyy-MM-dd"));
			writer.WriteEndElement();
		}
		public void WriteItem(string link, DateTime publishedDate, string priority, string changefreq)
		{
			writer.WriteStartElement("url");
			writer.WriteElementString("loc", link);
			writer.WriteElementString("lastmod", publishedDate.ToString("yyyy-MM-dd"));
			writer.WriteElementString("changefreq", changefreq);
			writer.WriteElementString("priority", priority);
			writer.WriteEndElement();
		}
	}
}

