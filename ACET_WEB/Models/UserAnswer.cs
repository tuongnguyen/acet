﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Models
{
    public class UserAnswerForm 
    {
        public string name { get; set; }
        public string email { get; set; }
        public List<List<string>> user_anser { get; set; }
       
    }
}
