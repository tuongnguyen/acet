﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Models
{

    public class CategoryForm
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string name_en { get; set; }
        public string img { get; set; }
        public string img_mobile { get; set; }
        public string img_link { get; set; }
		public string img_link_en { get; set; }
    }
}