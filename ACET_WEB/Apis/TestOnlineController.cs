﻿using ACET_WEB.App_Start;
using ACET_WEB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ACET_WEB.Apis
{
    public class TestOnlineController : ApiController
    {
        public WrapTestOnlineSerial GetList([FromUri] int cus_type)
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var rs = new WrapTestOnlineSerial();
               
                var rs_data_all = new List<List<List<dynamic>>>();
                if (cus_type > 0)
                {
                    var question_group = dc.QuestionGroups.Where(q=>q.Type == cus_type).OrderBy(r => Guid.NewGuid()).Take(1).First();
                    foreach (var qu in question_group.Questions.ToList())
                    {
                        var rs_data = new List<List<dynamic>>();
                        var item_q = new List<dynamic>();
                        item_q.Add(qu.Id);
                        item_q.Add(qu.Question1);
                        rs_data.Add(item_q);
                        foreach (var ans in qu.Answers.ToList())
                        {
                            var item = new List<dynamic>();
                            item.Add(ans.Answer1);
                            item.Add(ans.Name);                         
                            rs_data.Add(item);
                        }
                        rs_data_all.Add(rs_data);
                    }
                    rs.draw = cus_type;
                    rs.groupQuestion = question_group.Desc;
                    rs.data = rs_data_all;
                    return rs;
                }
               
                else
                {
                    rs.draw = cus_type;
                    rs.groupQuestion = "";
                    rs.data = rs_data_all;
                    return rs;
                }
            }
        }
        [HttpPost]
		public string[] SendMail(UserAnswerForm data)
        {
            string body;
            //Read template file from the App_Data folder
            using (var sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("\\App_Data\\Templates\\") + "MailTestonline.txt"))
            {
                body = sr.ReadToEnd();
            }

            try
            {
                //add email logic here to email the customer that their invoice has been voided
                //Username: {1}
                string username = data.name;
                int point = 0;
				foreach(List<string> q in data.user_anser){
					using (acet_dbEntities dc = new acet_dbEntities())
					{
						int qId = Int32.Parse(q[0]);
						var qs = dc.Questions.FirstOrDefault(p => p.Id == qId);
						if(qs!=null){
							if(qs.AnswerKey.Equals(q[1].Trim())){
								point++;
							}
						}
					}
				}
//                string sender = ConfigurationManager.AppSettings["EmailFromAddress"];
//                string emailSubject = @"TestOnline Result";
//                string recipient = data.email;
//                string messageBody = string.Format(body, username, point);
//
//                var MailHelper = new MailHelper
//                {
//                    Sender = sender, //email.Sender,
//                    Recipient = recipient,
//                    RecipientCC = null,
//                    Subject = emailSubject,
//                    Body = messageBody
//                };
//                MailHelper.Send();

                //return RedirectToAction("EmailConfirm");

				return new string[]{point.ToString(), data.name};
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
				return new string[]{e.Message};
            }
            
        }
    }
}
