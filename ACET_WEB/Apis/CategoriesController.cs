﻿using ACET_WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using ExtensionMethods;
using System.Data.Entity.Core.Objects.DataClasses;
namespace ACET_WEB.Apis
{
    public class CategoriesController : ApiController
    {
        //
        // GET: /Categories/
        public WrapUserSerial GetAll([FromUri] int draw, [FromUri] int length, [FromUri] int start,
                 [FromUri] string search_value, [FromUri] string order_colunm, [FromUri] string order_dir, [FromUri] string cus_type)
        {

            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var rs = new WrapUserSerial();
                var rs_data = new List<List<dynamic>>();
                var users = dc.Categories.Where(c => c.Status==1);
                if (search_value != null)
                {
                    if (!search_value.Trim().Equals(""))
                    {
                        users = users.Where(c => c.Name.Contains(search_value) || c.NameEng.Contains(search_value));
                    }
                }
                switch (order_colunm)
                {
                    case "0":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }
                        break;
                    case "1":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Name);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Name);
                        }
                        break;
                    case "2":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.NameEng);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.NameEng);
                        }
                        break;
                    case "3":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Modified);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Modified);
                        }
                        break;
                    default:
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }

                        break;
                }
                var recordsTotal = users.Count();
                users = users.Skip(start).Take(length);
                foreach (var user in users.ToArray())
                {
                    var item = new List<dynamic>();
                    item.Add(user.Id.ToString());
                    item.Add(user.Name);
                    item.Add(user.NameEng);
                    item.Add(user.Modified.ToString());
                    rs_data.Add(item);
                }
                rs.draw = draw;
                rs.recordsFiltered = recordsTotal;
                rs.recordsTotal = recordsTotal;
                rs.data = rs_data;
                return rs;
            }
        }

    }
}
