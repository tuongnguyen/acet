﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ACET_WEB.Models
{

    public class MenuTree
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string NameEng { get; set; }
        public bool HasChild { get; set; }
        public DateTime Modified { get; set; }
        public List<MenuTree> Childs { get; set; }
        public int Order { get; set; }
		public int ParentId { get; set; }
		public string Link { get; set; }
		public string LinkEng { get; set; }
		public string Type { get; set; }
    }
   
}